import 'package:flutter/material.dart';

class MyAccount extends StatefulWidget {
  @override
  _MyAccountState createState() => _MyAccountState();
}

class _MyAccountState extends State<MyAccount> {
  String firstName = '';
  String lastName = '';
  String email = '';
  String numTel = '';
  String password = '';
  String username = '';

  final _formKey = GlobalKey<FormState>();

  void showSnackBarMessage(String message, [MaterialColor color = Colors.red]) {
    Scaffold.of(context).showSnackBar(new SnackBar(content: new Text(message)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('New Account'),
        backgroundColor: Colors.blue,
        actions: <Widget>[
          RaisedButton(
            onPressed: () {
              final FormState formState = _formKey.currentState;

              if (!formState.validate()) {
                showSnackBarMessage('Please enter correct data');
              } else {
                formState.save();
                print("Name: $firstName");
                print("Phone: $lastName");
                print("Email: $email");
                print("Name: $numTel");
                print("Phone: $username");

                showDialog(
                    context: context,
                    child: new AlertDialog(
                      title: new Text("Details"),
                      //content: new Text("Hello World"),
                      content: new SingleChildScrollView(
                        child: new ListBody(
                          children: <Widget>[
                            new Text("Name : " + firstName),
                            new Text("Phone : " + lastName),
                            new Text("Email : " + email),
                            new Text("Name : " + numTel),
                            new Text("Phone : " + username)
                          ],
                        ),
                      ),
                      actions: <Widget>[
                        new FlatButton(
                          child: new Text('OK'),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    ));
              }
            },
            color: Colors.green,
            child: Text(
              'Save',
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 30.0),
              child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {},
                            child: CircleAvatar(
                              radius: 40.0,
                              backgroundImage: AssetImage("assets/img.jpg"),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 5.0),
                      TextFormField(
                        decoration: InputDecoration(
                            labelText: 'Votre Prénom',
                            prefixIcon: const Icon(Icons.person),
                            border: OutlineInputBorder()),
                        validator: (val) =>
                            val.isEmpty ? 'Entrez un prénom' : null,
                        onChanged: (val) => firstName = val,
                      ),
                      SizedBox(height: 5.0),
                      TextFormField(
                        decoration: InputDecoration(
                            labelText: 'Votre Nom',
                            prefixIcon: const Icon(Icons.person),
                            border: OutlineInputBorder()),
                        validator: (val) =>
                            val.isEmpty ? 'Entrez un nom' : null,
                        onChanged: (val) => lastName = val,
                      ),
                      SizedBox(height: 5.0),
                      TextFormField(
                        decoration: InputDecoration(
                            labelText: 'Votre Email',
                            prefixIcon: const Icon(Icons.email),
                            border: OutlineInputBorder()),
                        keyboardType: TextInputType.emailAddress,
                        validator: (val) => val.isEmpty || !val.contains('@')
                            ? 'Entrez un email'
                            : null,
                        onChanged: (val) => email = val,
                      ),
                      SizedBox(height: 5.0),
                      TextFormField(
                        decoration: InputDecoration(
                            labelText: 'Votre Téléphone',
                            prefixIcon: const Icon(Icons.phone),
                            border: OutlineInputBorder()),
                        keyboardType: TextInputType.phone,
                        validator: (val) => val.isEmpty
                            ? 'Entrez un numero de telephone'
                            : null,
                        onChanged: (val) => numTel = val,
                      ),
                      SizedBox(height: 5.0),
                      TextFormField(
                        decoration: InputDecoration(
                            labelText: 'Votre Login',
                            prefixIcon: const Icon(Icons.account_circle),
                            border: OutlineInputBorder()),
                        validator: (val) =>
                            val.isEmpty ? 'Entrez un username' : null,
                        onChanged: (val) => username = val,
                      ),
                      SizedBox(height: 5.0),
                      TextFormField(
                        obscureText: true,
                        decoration: InputDecoration(
                            labelText: 'Votre Password',
                            prefixIcon: const Icon(Icons.lock),
                            border: OutlineInputBorder()),
                        validator: (val) =>
                            val.isEmpty ? 'Entrez un mot de passe' : null,
                        onChanged: (val) => password = val,
                      ),
                      SizedBox(height: 5.0),
                      TextFormField(
                        obscureText: true,
                        decoration: InputDecoration(
                            labelText: 'Confirm Password',
                            prefixIcon: const Icon(Icons.lock),
                            border: OutlineInputBorder()),
                        validator: (val) =>
                            val.isEmpty ? 'Entrez même mot de passe' : null,
                        onChanged: (val) => password = val,
                      ),
                    ],
                  )),
            ),
          )
        ],
      ),
    );
  }
}
