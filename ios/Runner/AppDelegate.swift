import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GeneratedPluginRegistrant.register(with: self)
      // TODO: Add your API key
    GMSServices.provideAPIKey("AIzaSyABaZ5XxfYV9tP3I5RXEFKUOX9EYgmlKjY")

    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
